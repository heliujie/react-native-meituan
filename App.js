

import React, { PureComponent } from 'react'
// 在使用Navigator时，针对不同的路由指定不同的状态栏样式
import { StatusBar, TabBarIOS } from 'react-native'
// API https://reactnavigation.org/docs/intro/quick-start
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import color from './src/widget/color'
import HomeScene from './src/scene/Home/HomeScene'
import NearbyScene from './src/scene/Nearby/NearbyScene'
import OrderScene from './src/scene/Order/OrderScene'
import MineScene from './src/scene/Mine/MineScene'
import WebScene from './src/widget/webScene'
import GroupPurchaseScene from './src/scene/GroupPurchase/GroupPurchase'
import TabBarItem from './src/widget/TabBarItem'

function getCurrentRouteName(navigationState) {
  if (!navigationState) {
      return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
      return getCurrentRouteName(route);
  }
  return route.routeName;
}

class RootScene extends PureComponent {
  constructor() {
      super()

      StatusBar.setBarStyle('dark-content')
  }

  render() {
    return (
      <Navigator 
        onNavigationStateChange={
          ({prevState, currentState}) => {
            const currentScene = getCurrentRouteName(currentState);
            const previousScene = getCurrentRouteName(prevState);
            console.log('prevState', prevState, 'currentState', currentState)
            console.log('currentScene', currentScene, 'previousScene', previousScene)
            if (previousScene !== currentScene) {
                if (lightContentScenes.indexOf(currentScene) >= 0) {
                    StatusBar.setBarStyle('light-content')
                } else {
                    StatusBar.setBarStyle('dark-content')
                }
            }
          }
        }
      />
    )
  }
}

const Tab = TabNavigator(
  {
      Home: {
          screen: HomeScene,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: '团购',
              tabBarIcon: ({ focused, tintColor }) => (
                <TabBarItem 
                  tintColor={tintColor}
                  focused={focused}
                  normalImage={require('./src/img/tabbar/pfb_tabbar_homepage.png')}
                  selectedImage={require('./src/img/tabbar/pfb_tabbar_homepage_selected.png')}
                />
              )
          }),
      },
      Nearby: {
          screen: NearbyScene,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: '附近',
              tabBarIcon: ({ focused, tintColor }) => (
                  <TabBarItem
                      tintColor={tintColor}
                      focused={focused}
                      normalImage={require('./src/img/tabbar/pfb_tabbar_merchant.png')}
                      selectedImage={require('./src/img/tabbar/pfb_tabbar_merchant_selected.png')}
                  />
              )
          }),
      },

      Order: {
          screen: OrderScene,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: '订单',
              tabBarIcon: ({ focused, tintColor }) => (
                  <TabBarItem
                      tintColor={tintColor}
                      focused={focused}
                      normalImage={require('./src/img/tabbar/pfb_tabbar_order.png')}
                      selectedImage={require('./src/img/tabbar/pfb_tabbar_order_selected.png')}
                  />
              )
          }),
      },

      Mine: {
          screen: MineScene,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: '我的',
              tabBarIcon: ({ focused, tintColor }) => (
                  <TabBarItem
                      tintColor={tintColor}
                      focused={focused}
                      normalImage={require('./src/img/tabbar/pfb_tabbar_mine.png')}
                      selectedImage={require('./src/img/tabbar/pfb_tabbar_mine_selected.png')}
                  />
              )
          }),
      },
  },
  {
      tabBarComponent: TabBarBottom,
      tabBarPosition: 'bottom',
      swipeEnabled: true,
      animationEnabled: true,
      lazy: true,
      tabBarOptions: {
          activeTintColor: color.theme,
          inactiveTintColor: '#979797',
          style: { backgroundColor: '#ffffff' },
      },
  }

);

const Navigator = StackNavigator(
  {
      Tab: { screen: Tab },
      Web: { screen: WebScene },
      GroupPurchase: { screen: GroupPurchaseScene },
  },
  {
      navigationOptions: {
          headerStyle: { backgroundColor: color.theme },
          headerBackTitle: null,
          headerTintColor: '#333333',
          showIcon: true,
      },
  }
);

export default RootScene;