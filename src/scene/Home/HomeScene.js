import React, { PureComponent } from 'react'
import api from '../../../api'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ListView, Image, StatusBar, FlatList } from 'react-native'
import { Heading1, Heading2, Paragraph } from '../../widget/Text'
import color from '../../widget/color'
import { screen, system } from '../../common'
import GroupPurchaseCell from '../GroupPurchase/GroupPurchaseCell'
import SpacingView from '../../widget/SpacingView'

import HomeMenuView from './HomeMenuView'
import HomeGridView from './HomeGridView'


class HomeScene extends PureComponent {

    state: {
        discounts: Array<Object>,
        dataList: Array<Object>,
        refreshing: boolean
    }

    constructor(props: Object) {
        super(props)

        this.state = {
            discounts: [],
            dataList: [],
            refreshing: false,
        }

    }

    componentDidMount() {
        this.requestData()
    }

    requestData() {
        this.setState({
            refreshing: true
        })
        this.requestDiscount()
        this.requestRecommend()
    }

    async requestRecommend() {
        try{
            let response = await fetch(api.recommend)
            let json = await response.json()
            let dataList = json.data.map(
                (info) => {
                    return {
                        id: info.id,
                        imageUrl: info.squareimgurl,
                        title: info.mname,
                        subtitle: `[${info.range}]${info.title}`,
                        price: info.price
                    }
                }
            )
            this.setState({
                dataList: dataList,
                refreshing: false,
            })
            console.log('dataList', dataList)
        } catch (error){
            this.setState({ refreshing: false })
        }
    }

    async requestDiscount() {
        try {
            let response = await fetch(api.discount)
            let json = await response.json()
            this.setState({ discounts: json.data })
            console.log('discounts', this.state.discounts)
        } catch (error) {
            alert(error)
        }
    }

    onMenuSelected(index: number) {
        alert(index)
    }

    onGridSelected(index: number) {
        let discount = this.state.discounts[index]

        if (discount.type == 1) {
            StatusBar.setBarStyle('default', false)

            let location = discount.tplurl.indexOf('http')
            let url = discount.tplurl.slice(location)
            console.log('url', url)
            this.props.navigation.navigate('Web', { url: url })
        }
    }

    renderHeader() {
        return (
            <View>
                <HomeMenuView menuInfos={api.menuInfo} onMenuSelected={this.onMenuSelected} />

                <SpacingView />
                <HomeGridView infos={this.state.discounts} onGridSelected={(this.onGridSelected).bind(this)} /> 

                <SpacingView />

                <View style={styles.recommendHeader}>
                    <Heading2>猜你喜欢</Heading2>
                </View>
            </View>
        )
    }

    renderCell(info: Object) {
        return (
            <GroupPurchaseCell
                // info={info.item}
                // onPress={this.onCellSelected}
            />
        )
    }

    keyExtractor(item: Object, index: number) {
        return item.id
    }
    
    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.dataList}
                    keyExtractor={this.keyExtractor}
                    onRefresh={this.requestData.bind(this)}
                    refreshing={this.state.refreshing}
                    ListHeaderComponent={this.renderHeader.bind(this)}
                    renderItem={this.renderCell.bind(this)}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.background
    },
    recommendHeader: {
        height: 35,
        justifyContent: 'center',
        borderWidth: screen.onePixel,
        borderColor: color.border,
        paddingVertical: 8,
        paddingLeft: 20,
        backgroundColor: 'white'
    },
    searchBar: {
        width: screen.width * 0.7,
        height: 30,
        borderRadius: 19,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        alignSelf: 'center',
    },
    searchIcon: {
        width: 20,
        height: 20,
        margin: 5,
    }
});

export default HomeScene;