import React, { PureComponent } from 'react'

import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ListView, Image, StatusBar, FlatList } from 'react-native'

class HomeScene extends PureComponent {
    constructor() {
        super()
    }

    render() {
        return (
            <View style={styles.center}>
                <Text>web scene</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    center: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    }
}) 

export default HomeScene;