### 依赖库

> package.json
```
{
  "name": "MeiTuan",
  "version": "0.0.1",
  "private": true,
  "scripts": {
    "start": "node node_modules/react-native/local-cli/cli.js start",
    "flow": "flow; test $? -eq 0 -o $? -eq 2",
    "test": "jest",
    "ios": "react-native run-ios",
    "android": "react-native run-android"
  },
  "dependencies": {
    "prop-types": "^15.6.0",
    "react": "^16.2.0",
    "react-native": "^0.51.0",
    "react-native-scrollable-tab-view": "^0.8.0",
    "react-navigation": "^1.0.0-beta.21"
  },
  "devDependencies": {
    "babel-jest": "^21.2.0",
    "babel-preset-react-native": "^4.0.0",
    "flow-bin": "^0.61.0",
    "jest": "^21.2.1",
    "react-test-renderer": "^16.2.0"
  },
  "jest": {
    "preset": "react-native"
  }
}
```
- 使用prop-types对组件中对props进行类型检测
 
- 需要一个更通用的Tab控制器， 使用[react-native-scrollable-tab-view](https://github.com/skv-headless/react-native-scrollable-tab-view)

- [react-navigation](https://github.com/react-community/react-navigation)是致力于解决导航卡顿，数据传递，Tabbar和navigator布局，支持redux 


## 底部Tab

> 效果

![](http://osbfr39w7.bkt.clouddn.com/Jietu20171218-101553.jpg)


```
...
render() {
    return(
        <Navigator />
    )
}
...
const Navigator = StackNavigator(
  {
      Tab: { screen: Tab },
      Web: { screen: WebScene },
      GroupPurchase: { screen: GroupPurchaseScene },
  },
  {
      navigationOptions: {
          headerStyle: { backgroundColor: color.theme },
          headerBackTitle: null,
          headerTintColor: '#333333',
          showIcon: true,
      },
  }
)
...

const Tab = TabNavigator(
  {
      Home: {
          screen: HomeScene,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: '团购',
              tabBarIcon: ({ focused, tintColor }) => (
                <TabBarItem 
                  tintColor={tintColor}
                  focused={focused}
                  normalImage={require('./src/img/tabbar/pfb_tabbar_homepage.png')}
                  selectedImage={require('./src/img/tabbar/pfb_tabbar_homepage_selected.png')}
                />
              )
          }),
      }
      ...
  }
)
```
其中TabBarItem组件主要是对image进行封装, 方便逻辑控制

是否处在当前tab, 组件提供参数focused用于判断。
```
render() {
    let selectedImage = this.props.selectedImage ? this.props.selectedImage : this.props.normalImage
    return (
        <Image
            source={this.props.focused
                ? selectedImage
                : this.props.normalImage}
            style={{ tintColor: this.props.tintColor, width: 25, height: 25 }}
        />
    );
}
```

其中tintColor颜色值是在配置时传递进去
```
const Navigator = StackNavigator(
  {
    ...
  },
  {
      navigationOptions: {
          headerStyle: { backgroundColor: color.theme },
          headerBackTitle: null,
          headerTintColor: '#333333',
          showIcon: true,
      },
  }
)
```

### 团购页内容组件

封装成组件传递给screen属性
```
...
screen: HomeScene,
...
```

> 页面效果图
![](http://osbfr39w7.bkt.clouddn.com/Jietu20171218-103419.jpg)

使用了FlatList组件， 高性能的简单列表组件，支持单独的头部组件，完全跨平台
[查看文档](http://reactnative.cn/docs/0.51/flatlist.html#content)
```
render() {
    return (
        <View style={styles.container}>
            <FlatList
                data={this.state.dataList}
                keyExtractor={this.keyExtractor}
                onRefresh={this.requestData.bind(this)}
                refreshing={this.state.refreshing}
                ListHeaderComponent={this.renderHeader.bind(this)}
                renderItem={this.renderCell.bind(this)}
            />
        </View>
    );
}
```
其中定义头部renderHeader
```
ListHeaderComponent={this.renderHeader.bind(this)}
```
负责的是这部分内容

![](http://osbfr39w7.bkt.clouddn.com/Jietu20171218-104145.jpg)

2部分大组件，抽出来，分别是HomeMenuView和HomeGridView

其中分隔间隙是通用的，抽出一个SpacingView组件

对应的函数是这样的
```
renderHeader() {
    return (
        <View>
            <HomeMenuView menuInfos={api.menuInfo} onMenuSelected={this.onMenuSelected} />

            <SpacingView />
            <HomeGridView infos={this.state.discounts} onGridSelected={(this.onGridSelected).bind(this)} /> 

            <SpacingView />

            <View style={styles.recommendHeader}>
                <Heading2>猜你喜欢</Heading2>
            </View>
        </View>
    )
}
```

### 轮播效果实现

使用了[ScrollView](http://reactnative.cn/docs/0.51/scrollview.html#content)组件控制横向滚动

数据通过父组件通过prop传递进来，渲染到menuViews中
```
<View style={styles.container}>
    <ScrollView contentContainerStyle={styles.contentContainer}
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        onScroll={(e) => this.onScroll(e)}
    >
        <View style={styles.menuContainer}>
            {menuViews}
        </View>
    </ScrollView>


    <PageControl
        style={styles.pageControl}
        numberOfPages={pageCount}
        currentPage={this.state.currentPage}
        hidesForSinglePage
        pageIndicatorTintColor='gray'
        currentPageIndicatorTintColor={color.theme}
        indicatorSize={{ width: 8, height: 8 }}
    />
</View>
```
分页器没有使用组件自带的，在这里自己简单实现一个PageControl组件

监听滚动事件onScroll

计算当前页，这里通过e.nativeEvent.contentOffset.x来间接获取到，不断的更改currentPage属性
```
onScroll(e: any) {
    let x = e.nativeEvent.contentOffset.x
    let currentPage = Math.round(x / screen.width)

    console.log('onScroll  ' + e.nativeEvent.contentOffset.x + '  page ' + currentPage + '  current ' + this.state.currentPage)
    if (this.state.currentPage != currentPage) {
        this.setState({
            currentPage: currentPage
        })
    }
}
```

### 组件分类

![](http://osbfr39w7.bkt.clouddn.com/Jietu20171218-110046.jpg)

从整体到局部再到一个单元进行划分，最大程度的提高组件复用率
